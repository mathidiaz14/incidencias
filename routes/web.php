<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControladorUsuario;
use App\Http\Controllers\ControladorIncidencia;
use App\Http\Controllers\ControladorPrincipal;
use App\Http\Controllers\ControladorComentario;
use App\Http\Controllers\ControladorReclamo;
use App\Http\Controllers\ControladorCategoria;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/register', function () {
    return redirect('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    if(\Auth::user()->tipo == "Administrador")
    	return view('admin.reclamos');
    else
    	return view('usuario.incidencias');

})->name('dashboard');


Route::resource('reclamos'                          , ControladorReclamo::class);
Route::resource('usuarios'                          , ControladorUsuario::class)->middleware('auth.admin');
Route::resource('comentarios'                       , ControladorComentario::class)->middleware('auth');
Route::resource('categorias'                        , ControladorCategoria::class)->middleware('auth');

Route::get('usuario/incidencias', [ControladorPrincipal::class ,'incidencias'])->middleware('auth');
Route::resource('incidencias', ControladorIncidencia::class)->middleware('auth');
Route::delete('cerrar/incidencia', [ControladorIncidencia::class ,'cerrar_incidencia'])->middleware('auth');
Route::get('abrir/incidencia/{ID}', [ControladorIncidencia::class ,'abrir_incidencia'])->middleware('auth.admin');
Route::get('incidencias/usuario/{ID}', [ControladorIncidencia::class ,'incidencias_usuario'])->middleware('auth.admin');
Route::get('inactivas/incidencias', [ControladorIncidencia::class, 'incidencias_inactivas'])->middleware('auth.admin');

Route::get('usuario', [ControladorPrincipal::class ,'ver_usuario'])->middleware('auth');
Route::post('usuario', [ControladorPrincipal::class ,'guardar_usuario'])->middleware('auth');
Route::post('buscar', [ControladorPrincipal::class ,'buscar'])->middleware('auth');

Route::get('reportes', [ControladorPrincipal::class ,'get_reportes'])->middleware('auth');
Route::post('reportes', [ControladorPrincipal::class ,'set_reportes'])->middleware('auth');
Route::post('pdf/reportes', [ControladorPrincipal::class ,'pdf_reportes'])->middleware('auth');

Route::get('email', function () 
{
	$contenido = 'Este es el contenido de la prueba';

	return enviar_email('prueba', 'Prueba de envio de email', $contenido, 'Mathias Díaz', 'diaz.mathias.14@gmail.com');
});
