@extends('layouts.auth')

@section('content')
<div class="login-wrap">
    <div class="login-content">
        <div class="login-logo">
            <a href="#">
                <b>{{env('APP_EMPRESA')}}</b>
            </a>
        </div>
        @error('email')
            <span role="alert" style="  color: #721c24;
                                        background-color: #f8d7da;
                                        border-color: #f5c6cb;
                                        padding: .75rem 1.25rem;
                                        margin-bottom: 1rem;
                                        border: 1px solid transparent;
                                        border-radius: .25rem;
                                        width: 100%;">
                <strong>{{ $message }}</strong>
            </span>
            <br><br>
        @enderror
        @error('password')
            <span role="alert" style="  color: #721c24;
                                        background-color: #f8d7da;
                                        border-color: #f5c6cb;
                                        padding: .75rem 1.25rem;
                                        margin-bottom: 1rem;
                                        border: 1px solid transparent;
                                        border-radius: .25rem;
                                        width: 100%;">
                <strong>{{ $message }}</strong>
            </span>
            <br><br>
        @enderror
        <div class="login-form">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label>Dirección de correo</label>
                    <input class="au-input au-input--full" type="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                </div>
                <div class="login-checkbox">
                    <label>
                        <input type="checkbox" name="remember">Recuerdame
                        
                    </label>
                    <label>
                        <a href="#">Olvide mi contraseña</a>
                    </label>
                </div>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Iniciar Sesión</button>
            </form>
            
        </div>
    </div>
</div>

@endsection
