@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 offset-md-2">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Incidencia numero {{$incidencia->id}}</h3>
                    	</div>
                    	<div class="table-data__tool-right">
                    		@if(($incidencia->estado == "activo") and ((Auth::user()->tipo == "Administrador") or ($incidencia->user_id == Auth::user()->id)))
	                    		<button class="btn btn-danger" data-toggle="modal" data-target="#cerrarIncidencia">
								    <i class="fa fa-times"></i>
								    Cerrar incidencia
								</button>

								<!-- Modal -->
								<div class="modal fade" id="cerrarIncidencia" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
									<div class="modal-dialog" role="document">
								  		<div class="modal-content">
											<div class="modal-header" style="background: #dc3545;">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
								    		</div>
								    		<div class="modal-body text-center">
								    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
								    			<br>
								      			<h4>¿Desea cerrar la incidencia Nº {{$incidencia->id}}?</h4>
								      			<br>
								      			<div class="row">
								      				<div class="col">
														<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
															NO
														</button>
								      				</div>
								      				<div class="col">
							      					<form action="{{url('cerrar/incidencia')}}" method="POST">
									                    @csrf
									                    <input type='hidden' name='_method' value='DELETE'>
									                    <input type="hidden" name="incidencia" value="{{$incidencia->id}}">
									                    <button class="btn btn-danger btn-block">
									                        SI
									                    </button>
									                </form>
								      				</div>
								      			</div>
								    		</div>
								  		</div>
									</div>
								</div>
	                    	@else
	                    		@if(Auth::user()->tipo == "Administrador")
	                    			<a href="{{url('abrir/incidencia', $incidencia->id)}}" class="btn btn-success">
	                    				<i class="fa fa-folder-open"></i>
	                    				Reabrir incidencia
	                    			</a>
	                    		@endif
	                    	@endif
                    	</div>	
                    </div>
                    @include('ayuda.alerta')
                    <form class="form-horizontal">
                    	<div class="row">
                    		<div class="col-xs-12 col-md-6">
		                    	<div class="form-group">
		                    		<label for="" class="form-label">
		                    			Titulo
		                    		</label>
		                    		<input type="text" class="form-control" readonly value="{{$incidencia->titulo}}">
		                    	</div>
                    		</div>
                    		<div class="col-xs-12 col-md-6">
		                    	<div class="form-group">
		                    		<label for="" class="form-label">
		                    			Fecha
		                    		</label>
		                    		<input type="text" class="form-control" readonly value="{{$incidencia->created_at->format('d/m/Y H:i')}}">
		                    	</div>
                    		</div>
                    		<div class="col-xs-12 col-md-6">
		                    	<div class="form-group">
		                    		<label for="" class="form-label">
		                    			Asignado a
		                    		</label>
		                    		<input type="text" class="form-control" readonly value="{{isset($incidencia->usuario) ? $incidencia->usuario->name : $incidencia->usuario_asignado}}">
		                    	</div>
                    		</div>
                    		<div class="col-xs-12 col-md-6">
		                    	<div class="form-group">
		                    		<label for="" class="form-label">
		                    			Categoria
		                    		</label>
		                    		<input type="text" class="form-control" readonly value="{{$incidencia->categoria_id != null ? $incidencia->categoria->nombre : '--'}}">
		                    	</div>
                    		</div>
                    		<div class="col-xs-12 col-md-12">
	                    		<div class="form-group">
	                    			<label for="" class="form-label">
	                    				Contenido
	                    			</label>
	                    			<textarea class="form-control" readonly="" rows="6">{{$incidencia->contenido}}</textarea>
	                    		</div>
                    		</div>
                    	</div>
                    </form>
                    <hr>
                    <div class="table-data__tool-left">
                		<h3 class="title-5 m-b-35">Historial</h3>
                	</div>
                	
                	@if($incidencia->comentarios->count() == 0)
                		<div class="col-xs-12 text-center m-b-35">
                			<p style="opacity: .6;">No hay ningun dato</p>
                		</div>
                	@endif

                		<div class="table-responsive table-responsive-data2">
	                        <table class="table table-data2">
	                            <thead>
	                                <tr>
	                                    <th>Usuario</th>
	                                    <th>Fecha</th>
	                                    <th>Contenido</th>
	                                    @if(Auth::user()->tipo == "Administrador")	
	                                        <th></th>
	                                    @endif
	                                </tr>
	                            </thead>
	                            <tbody>
	                                @foreach($incidencia->comentarios as $comentario)
	                                	<tr class="tr-shadow">
		                                    <td>
		                                        <span class="block-email">
	                                    			@if($comentario->user_id != null) 
				                						{{$comentario->usuario->name}} 
					                				@else
					                					{{$comentario->usuario_asignado}}
					                				@endif
		                                        </span>
		                                    </td>
		                                    <td>
		                                    	{{$comentario->created_at->format('d/m/Y H:i')}}
		                                    </td>
	                                        <td>
	                                        	{{$comentario->contenido}}
	                                        </td>
                                            @if(Auth::user()->tipo == "Administrador")	
	                                        	@if($incidencia->estado == 'activo')
		                                        	<td>
														 @include('ayuda.eliminar', ['id' => $comentario->id, 'ruta' => url('comentarios', $comentario->id)])
			                                    	</td>
	                                        	@else
	                                        		<td>
														<a href="" class="btn btn-danger disabled">
															<i class="fa fa-trash"></i>
															Eliminar
														</a>
			                                    	</td>
	                                        	@endif
											@endif
		                                </tr>
	                                @endforeach
	                            </tbody>
	                        </table>
	                    </div>
                	<hr>
                	<div class="table-data__tool-left">
                		<h3 class="title-5 m-b-35 m-t-35">Deja un comentario para esta incidencia</h3>
                	</div>

                	<div class="col-xs-12">
                		@if($incidencia->estado == "activo")
                			<form action="{{url('comentarios')}}" method="post" class="form-horizontal">
	                			@csrf
	                			<div class="form-group">
		                			<input type="hidden" name="incidencia" value="{{$incidencia->id}}">
		                			<textarea name="comentario" id="" rows="3" class="form-control" required=""></textarea>
	                			</div>
	                			<div class="form-group text-right">
		                			<button class="btn btn-primary">
		                				<i class="fa fa-send"></i>
		                				Enviar
		                			</button>
	                			</div>
	                		</form>	
                		@else
                			<form action="" method="post" class="form-horizontal">
	                			<div class="form-group">
		                			<textarea name="comentario" id="" rows="3" class="form-control" required="" disabled="">No se puede comentar si la incidencia esta inactiva</textarea>
	                			</div>
	                			<div class="form-group text-right">
		                			<button class="btn btn-primary" disabled="">
		                				<i class="fa fa-send"></i>
		                				Enviar
		                			</button>
	                			</div>
	                		</form>	
                		@endif
                	</div>

                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection