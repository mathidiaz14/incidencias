@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <form action="{{url('buscar')}}" method="post" class="form-inline">
                                @csrf
                                <div class="form-group mx-sm-3 mb-2">
                                    <input type="text" class="form-control" name="buscar" placeholder="Buscar incidencia...">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">
                                    <i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">
                                Incidencias
                            </h3>
                        </div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#agregarUsuario">
							  <i class="fa fa-plus"></i>
							  Agregar incidencia
							</button>

							<!-- Modal -->
							<div class="modal fade" id="agregarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-lg" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Agregar incidencia</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <form action="{{url('incidencias')}}" method="post" class="form-horizontal">
							        	@csrf
							        	<div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="titulo" class=" form-control-label">Titulo</label>
                                                    <input required="" type="text" id="titulo" placeholder="Ingrese aqui el titulo" name="titulo" class="form-control">
                                                </div>
                                            </div>
                                        </div>     
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="titulo" class=" form-control-label">Nombre del afectado</label>
                                                    <input type="text" id="titulo" placeholder="Ingrese aqui el nombre del afectado" name="cliente_nombre" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="telefono" class=" form-control-label">Teléfono del afectado</label>
                                                    <input type="text" id="telefono" placeholder="Ingrese aqui el telefono del afectado" name="cliente_telefono" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="titulo" class=" form-control-label">Email del afectado</label>
                                                    <input type="email" id="titulo" placeholder="Ingrese aqui el email del afectado" name="cliente_email" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="edificio" class=" form-control-label">Edificio</label>
                                                    <select name="edificio" id="" class="form-control">
                                                        <option value="">Otro</option>
                                                        @foreach(App\Models\Edificio::all() as $edificio)
                                                            <option value="{{$edificio->id}}">{{$edificio->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="contraseña" class=" form-control-label">¿A quien se le asigna?</label>
                                                    <select name="usuario" id="" class="form-control">
                                                        <option value="">Otro</option>
                                                        @foreach(App\Models\User::all() as $usuario)
                                                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>  
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="categoria" class=" form-control-label">Catgeoria de la incidencia</label>
                                                    <select name="categoria" id="" class="form-control">
                                                        <option value="">Ninguna</option>
                                                        @foreach(App\Models\Categoria::all() as $categoria)
                                                            <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class=" form-control-label">Contenido</label>
                                            <textarea class="form-control" name="contenido" placeholder="Ingrese aqui el contenido" rows="5"></textarea>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <small>Si desean asignar el trabajo a un usuario no registrado, deben ingresarlo en el siguiente campo</small>
                                            <label for="usuario_asignado" class=" form-control-label">Nombre de usuario no registrado</label>
                                            <input type="text" id="usuario_asignado" placeholder="Ingrese aqui el nombre" name="usuario_asignado" class="form-control">
                                        </div>
										<div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">
									        	<i class="fa fa-chevron-left"></i>
									        	Cerrar
									        </button>
									        <button class="btn btn-primary">
									        	<i class="fa fa-save"></i>
									        	Guardar
									        </button>
								      	</div>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                    </div>

                    @include('ayuda.alerta')
                    
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Titulo</th>
                                    <th>Comentarios</th>
                                    <th>Edificio</th>
                                    <th>Categoria</th>
                                    <th>Asignado a</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($incidencias as $incidencia)
                                	<tr class="tr-shadow">
	                                    <td>{{$incidencia->id}}</td>
                                        <td>{{$incidencia->titulo}}</td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->comentarios->count()}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->edificio_id != null ? $incidencia->edificio->nombre : "--"}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->categoria_id != null ? $incidencia->categoria->nombre : "--"}}
                                            </span>
                                        </td>
	                                    <td>
                                        	<span class="block-email">
	                                    		@if(isset($incidencia->usuario))
	                                        		{{$incidencia->usuario->name}}
                                        		@else
                                        			{{$incidencia->usuario_asignado}}
                                        		@endif
                                        	</span>
	                                    </td>
                                        <td>
                                            @if($incidencia->estado == "activo")
                                                <span class="block-email" style="background:#b7eac4;">
                                                    Activa
                                                </span>
                                            @else
                                                <span class="block-email">
                                                    Inactiva
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->created_at->format('d/m/Y H:i')}}
                                            </span>
                                        </td>
                                        <td>
                                        	<a href="{{url('incidencias', $incidencia->id)}}" class="btn btn-success">
                                        		<i class="fa fa-eye"></i>
                                        		Ver
                                        	</a>
                                        </td>
	                                    <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#editarActivo{{$incidencia->id}}">
                                                <i class="fa fa-edit"></i>
                                                Editar
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="editarActivo{{$incidencia->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar incidencia</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('incidencias', $incidencia->id)}}" method="post" class="form-horizontal">
											        	@csrf
											        	@method('PATCH')
											        	<div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Titulo</label>
                                                                    <input required="" type="text" id="titulo" name="titulo" class="form-control" value="{{$incidencia->titulo}}">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Nombre del afectado</label>
                                                                    <input type="text" id="titulo" placeholder="Ingrese aqui el nombre del afectado" name="cliente_nombre" class="form-control" value="{{$incidencia->cliente_nombre}}">
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Teléfono del afectado</label>
                                                                    <input type="text" id="titulo" placeholder="Ingrese aqui el teléfono del afectado" name="cliente_telefono" class="form-control" value="{{$incidencia->cliente_telefono}}">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Email del afectado</label>
                                                                    <input type="email" id="titulo" placeholder="Ingrese aqui el email del afectado" name="cliente_email" class="form-control" value="{{$incidencia->cliente_email}}">
                                                                </div>
                                                            </div>
                                                        </div>
				                                        <div class="form-group">
				                                            <label for="email" class=" form-control-label">Contenido</label>
				                                            <textarea class="form-control" name="contenido" rows="5">{{$incidencia->contenido}}</textarea>
				                                        </div>
				                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="contraseña" class=" form-control-label">Usuario registrado</label>
                                                                    <select name="usuario" id="" class="form-control">
                                                                        @if($incidencia->user_id == null)
                                                                            <option value="" selected="">Otro</option>
                                                                        @else
                                                                            <option value="">Otro</option>
                                                                        @endif

                                                                        @foreach(App\Models\User::all() as $usuario)
                                                                            @if($incidencia->user_id == $usuario->id)
                                                                                <option value="{{$usuario->id}}" selected>{{$usuario->name}}</option>
                                                                            @else
                                                                                <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>  
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="contraseña" class=" form-control-label">Categoria</label>
                                                                    <select name="categoria" id="" class="form-control">
                                                                        @if($incidencia->categoria_id == null)
                                                                            <option value="" selected="">Ninguno</option>
                                                                        @else
                                                                            <option value="">Ninguno</option>
                                                                        @endif

                                                                        @foreach(App\Models\Categoria::all() as $categoria)
                                                                            @if($incidencia->categoria_id == $categoria->id)
                                                                                <option value="{{$categoria->id}}" selected>{{$categoria->nombre}}</option>
                                                                            @else
                                                                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>    
                                                            </div>
                                                        </div>
				                                        <hr>
														<div class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">
													        	<i class="fa fa-chevron-left"></i>
													        	Cerrar
													        </button>
													        <button class="btn btn-primary">
													        	<i class="fa fa-save"></i>
													        	Guardar
													        </button>
												      	</div>
											        </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $incidencia->id, 'ruta' => url('incidencias', $incidencia->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 mt-4">
                    @if($incidencias->lastPage() > 1)
                        <div class="row">
                          <div class="col text-center">
                            <a href="{{$incidencias->previousPageUrl()}}" class="btn btn-dark">
                              <i class="fa fa-chevron-left"></i>
                            </a>
                            
                            @for($i=1;$i<=$incidencias->lastPage();$i++)
                              @if($incidencias->currentPage() == $i)
                                <a href="{{$incidencias->url($i)}}" class="btn btn-info">
                                  {{$i}}
                                </a>
                              @else
                                @if(($i == 1) or ($i == 2) or ($i == 3) or ($i == $incidencias->lastPage() - 2) or ($i == $incidencias->lastPage() - 1) or ($i == $incidencias->lastPage()) or ($i == $incidencias->currentPage() + 1) or ($i == $incidencias->currentPage() - 1))
                                  <a href="{{$incidencias->url($i)}}" class="btn btn-default">
                                    {{$i}}
                                  </a> 
                                @endif
                              @endif
                            @endfor

                            <a href="{{$incidencias->nextPageUrl()}}" class="btn btn-dark">
                              <i class="fa fa-chevron-right"></i>
                            </a>
                          </div>
                        </div>
                    @endif
                </div>  
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection