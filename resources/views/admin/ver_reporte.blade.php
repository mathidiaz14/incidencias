@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">
                                Reporte ({{count($incidencias)}} incidencias)
                            </h3>
                        </div>
                        <div class="table-data__tool-right">
                        	<form action="{{url('pdf/reportes')}}" method="post">
                        		@csrf
                        		<input type="hidden" name="inicio" value="{{$request->inicio}}">
                        		<input type="hidden" name="fin" value="{{$request->fin}}">
                        		<input type="hidden" name="categoria" value="{{$request->categoria}}">
                        		<input type="hidden" name="usuario" value="{{$request->usuario}}">
                        		<input type="hidden" name="edificio" value="{{$request->edificio}}">
                        		<input type="hidden" name="estado" value="{{$request->estado}}">
                        		
                        		<button class="btn btn-info">
                        			Generar PDF
                        		</button>
                        	</form>
                        </div>
                    </div>

                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Titulo</th>
                                    <th>Edificio</th>
                                    <th>Categoria</th>
                                    <th>Asignado a</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($incidencias as $incidencia)
                                	<tr class="tr-shadow">
	                                    <td>{{$incidencia->id}}</td>
                                        <td>{{$incidencia->titulo}}</td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->edificio_id != null ? $incidencia->edificio->nombre : "--"}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->categoria_id != null ? $incidencia->categoria->nombre : "--"}}
                                            </span>
                                        </td>
	                                    <td>
                                        	<span class="block-email">
	                                    		@if($incidencia->user_id != null)
	                                        		{{$incidencia->usuario->name}}
                                        		@else
                                        			{{$incidencia->usuario_asignado}}
                                        		@endif
                                        	</span>
	                                    </td>
                                        <td>
                                            @if($incidencia->estado == "activo")
                                                <span class="block-email" style="background:#b7eac4;">
                                                    Activa
                                                </span>
                                            @else
                                                <span class="block-email">
                                                    Inactiva
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->created_at->format('d/m/Y H:i')}}
                                            </span>
                                        </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection