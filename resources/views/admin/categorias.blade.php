@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Categorias de incidencias</h3>
                    	</div>
                        <div class="table-data__tool-right">
                        	<button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#agregarCategoria">
							  <i class="fa fa-plus"></i>
							  Agregar categoria
							</button>

							<!-- Modal -->
							<div class="modal fade" id="agregarCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Agregar Categoria</h5>
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
							        <form action="{{url('categorias')}}" method="post" class="form-horizontal">
							        	@csrf
							        	<div class="form-group">
                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                            <input required="" type="text" id="nombre" placeholder="Ingrese aqui el nombre" name="nombre" class="form-control">
                                        </div>
										<div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">
									        	<i class="fa fa-chevron-left"></i>
									        	Cerrar
									        </button>
									        <button class="btn btn-primary">
									        	<i class="fa fa-save"></i>
									        	Guardar
									        </button>
								      	</div>
							        </form>
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\Models\Categoria::all() as $categoria)
                                	<tr class="tr-shadow">
	                                    <td>{{$categoria->nombre}}</td>
	                                    <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#editarCategoria{{$categoria->id}}">
                                                <i class="fa fa-edit"></i>
                                                Editar
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="editarCategoria{{$categoria->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('categorias', $categoria->id)}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        @method('PATCH')
                                                        <div class="form-group">
                                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                                            <input required="" type="text" id="nombre" value="{{$categoria->nombre}}" name="nombre" class="form-control">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                <i class="fa fa-chevron-left"></i>
                                                                Cerrar
                                                            </button>
                                                            <button class="btn btn-primary">
                                                                <i class="fa fa-save"></i>
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $categoria->id, 'ruta' => url('categorias', $categoria->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection