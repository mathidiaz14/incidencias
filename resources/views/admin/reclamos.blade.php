@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <h3 class="title-5 m-b-35">Reclamos activos</h3>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Edificio</th>
                                    <th>Unidad</th>
                                    <th>Fecha</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\Models\Reclamo::all()->where('estado', 'activo') as $reclamo)
                                	<tr class="tr-shadow">
                                        <td>{{$reclamo->nombre}}</td>
                                        <td>{{$reclamo->edificio}}</td>
                                        <td>{{$reclamo->unidad}}</td>
                                        <td>{{$reclamo->created_at->format('d/m/Y H:i')}}</td>
	                                    <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#editarReclamo{{$reclamo->id}}">
                                                <i class="fa fa-eye"></i>
                                                Ver
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="editarReclamo{{$reclamo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Ver reclamo</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('reclamos', $reclamo->id)}}" method="post" class="form-horizontal">
											        	@csrf
											        	@method('PATCH')
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Nombre</label>
                                                                    <input type="text" id="titulo" name="titulo" class="form-control" value="{{$reclamo->nombre}}">
                                                                </div>
                                                            </div>
                                                        </div>
											        	<div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Email</label>
                                                                    <input required="" type="email" id="titulo" name="email" class="form-control" value="{{$reclamo->email}}">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Telefono</label>
                                                                    <input required="" type="text" id="titulo" name="telefono" class="form-control" value="{{$reclamo->telefono}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Edificio</label>
                                                                    <input required="" type="text" id="titulo" name="edificio" class="form-control" value="{{$reclamo->edificio}}">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Unidad</label>
                                                                    <input required="" type="text" id="titulo" name="unidad" class="form-control" value="{{$reclamo->unidad}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="titulo" class=" form-control-label">Reclamo</label>
                                                                    <textarea name="reclamo" id="" cols="30" rows="3" class="form-control">{{$reclamo->reclamo}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="" class="form-control-label">Asignar incidencia</label>
                                                                    <select name="usuario" id="" class="form-control">
                                                                        <option value="">Otro</option>
                                                                        @foreach(App\Models\User::all() as $usuario)
                                                                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <label for="" class="form-control-label">Categoria</label>
                                                                    <select name="categoria" id="" class="form-control">
                                                                        <option value="">Ninguno</option>
                                                                        @foreach(App\Models\Categoria::all() as $categoria)
                                                                            <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
														<div class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">
													        	<i class="fa fa-chevron-left"></i>
													        	Cerrar
													        </button>
													        <button class="btn btn-primary">
													        	<i class="fa fa-save"></i>
													        	Crear incidencia
													        </button>
												      	</div>
											        </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $reclamo->id, 'ruta' => url('reclamos', $reclamo->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection