@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <form action="{{url('buscar')}}" method="post" class="form-inline">
                                @csrf
                                <div class="form-group mx-sm-3 mb-2">
                                    <input type="text" class="form-control" name="incidencia" placeholder="Buscar incidencia...">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">
                                    <i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Incidencias del usuario {{$usuario->name}}</h3>
                    	</div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Titulo</th>
                                    <th>Asignado a</th>
                                    <th>Fecha</th>
                                    <th>Comentarios</th>
                                    <th>Estado</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($incidencias as $incidencia)
                                	<tr class="tr-shadow">
	                                    <td>{{$incidencia->id}}</td>
                                        <td>{{$incidencia->titulo}}</td>
	                                    <td>
                                        	<span class="block-email">
	                                    		@if($incidencia->user_id != null)
	                                        		{{$incidencia->usuario->name}}
                                        		@else
                                        			{{$incidencia->usuario_asignado}}
                                        		@endif
                                        	</span>
	                                    </td>
                                        <td>{{$incidencia->created_at->format('d/m/Y H:i')}}</td>
                                        <td>{{$incidencia->comentarios->count()}}</td>
                                        <td>
                                            @if($incidencia->estado == "activo")
                                                Activa
                                            @else
                                                Inactiva
                                            @endif
                                        </td>
                                        <td>
                                        	<a href="{{url('incidencias', $incidencia->id)}}" class="btn btn-success">
                                        		<i class="fa fa-eye"></i>
                                        		Ver
                                        	</a>
                                        </td>
	                                    <td>
                                            <button class="btn btn-info" data-toggle="modal" data-target="#editarInactiva{{$incidencia->id}}">
                                                <i class="fa fa-edit"></i>
                                                Editar
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="editarInactiva{{$incidencia->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Editar incidencia</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form action="{{url('incidencias', $incidencia->id)}}" method="post" class="form-horizontal">
											        	@csrf
											        	@method('PATH')
											        	<div class="form-group">
				                                            <label for="titulo" class=" form-control-label">Titulo</label>
				                                            <input required="" type="text" id="titulo" name="titulo" class="form-control" value="{{$incidencia->titulo}}">
				                                        </div>
				                                        <div class="form-group">
                                                            <label for="email" class=" form-control-label">Contenido</label>
                                                            <textarea class="form-control" name="contenido" rows="10">{{$incidencia->contenido}}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="contraseña" class=" form-control-label">Usuario registrado</label>
                                                            <select name="usuario" id="" class="form-control">
                                                                @if($incidencia->user_id == null)
                                                                    <option value="" selected="">Otro</option>
                                                                @else
                                                                    <option value="">Otro</option>
                                                                @endif

                                                                @foreach(App\Models\User::all() as $usuario)
                                                                    @if($incidencia->user_id == $usuario->id)
                                                                        <option value="{{$usuario->id}}" selected>{{$usuario->name}}</option>
                                                                    @else
                                                                        <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <small>Si desean asignar el trabajo a un usuario no registrado, deben ingresarlo en el siguiente campo</small>
                                                            <label for="usuario_asignado" class=" form-control-label">Nombre de usuario no registrado</label>
                                                            <input type="text" id="usuario_asignado" placeholder="Ingrese aqui el nombre" name="usuario_asignado" class="form-control">
                                                        </div>
														<div class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">
													        	<i class="fa fa-chevron-left"></i>
													        	Cerrar
													        </button>
													        <button class="btn btn-primary">
													        	<i class="fa fa-save"></i>
													        	Guardar
													        </button>
												      	</div>
											        </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </td>
                                        <td>
                                            @include('ayuda.eliminar', ['id' => $incidencia->id, 'ruta' => url('incidencias', $incidencia->id)])
	                                    </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$incidencias->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection