<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>{{env("APP_EMPRESA")}}</title>
    <style>
    	table, th, td {
		  border: 1px solid black;
		}

		th
		{
			background: #bbbbbb;
		}
    </style>	
</head>

<body>
	<h1>Reporte <small>({{count($incidencias)}} incidencias)</small></h1>
    <table style="width:100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>Titulo</th>
                <th>Edificio</th>
                <th>Categoria</th>
                <th>Asignado a</th>
                <th>Estado</th>
                <th>Creado</th>
            </tr>
        </thead>
        <tbody>
            @foreach($incidencias as $incidencia)
            	<tr class="tr-shadow">
                    <td>{{$incidencia->id}}</td>
                    <td>{{$incidencia->titulo}}</td>
                    <td>
                        <span class="block-email">
                            {{$incidencia->edificio_id != null ? $incidencia->edificio->nombre : "--"}}
                        </span>
                    </td>
                    <td>
                        <span class="block-email">
                            {{$incidencia->categoria_id != null ? $incidencia->categoria->nombre : "--"}}
                        </span>
                    </td>
                    <td>
                    	<span class="block-email">
                    		@if($incidencia->user_id != null)
                        		{{$incidencia->usuario->name}}
                    		@else
                    			{{$incidencia->usuario_asignado}}
                    		@endif
                    	</span>
                    </td>
                    <td>
                        @if($incidencia->estado == "activo")
                            <span class="block-email" style="background:#b7eac4;">
                                Activa
                            </span>
                        @else
                            <span class="block-email">
                                Inactiva
                            </span>
                        @endif
                    </td>
                    <td>
                        <span class="block-email">
                            {{$incidencia->created_at->format('d/m/Y H:i')}}
                        </span>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
<!-- end document-->
