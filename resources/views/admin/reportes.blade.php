@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Reportes</h3>
                    	</div>
                    </div>
                    @include('ayuda.alerta')
                </div>
                <div class="col-12 col-md-8 offset-md-2">
                	<form action="{{url('reportes')}}" method="post" class="form-horizontal">
                		@csrf
                		<div class="row">
                			<div class="col">
                				<div class="form-group">
		                			<label for="">Fecha inicio</label>
		                			<input type="date" class="form-control" name="inicio" required>
		                		</div>		
                			</div>
                			<div class="col">
		                		<div class="form-group">
		                			<label for="">Fecha fin</label>
		                			<input type="date" class="form-control" name="fin" required>
		                		</div>
                			</div>
                		</div>
                        <div class="row">
                            <div class="col">
                        		<div class="form-group">
                        			<label for="">Categoria</label>
                        			<select name="categoria" id="" class="form-control">
                        				<option value="todos">Todas</option>
                        				@foreach(App\Models\Categoria::all() as $categoria)
                        					<option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                        				@endforeach
                        			</select>
                        		</div>
                            </div>
                            <div class="col">
                        		<div class="form-group">
                        			<label for="">Usuario</label>
                        			<select name="usuario" id="" class="form-control">
                        				<option value="todos">Todos</option>
                        				@foreach(App\Models\User::all() as $usuario)
                        					<option value="{{$usuario->id}}">{{$usuario->name}}</option>
                        				@endforeach
                        			</select>
                        		</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Edificio</label>
                                    <select name="edificio" id="" class="form-control">
                                        <option value="todos">Todos</option>
                                        @foreach(App\Models\Edificio::all() as $edificio)
                                            <option value="{{$edificio->id}}">{{$edificio->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Estado</label>
                                    <select name="estado" id="" class="form-control">
                                        <option value="todos">Todos</option>
                                        <option value="activo">Activas</option>
                                        <option value="inactivo">Inactivas</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">
                                <i class="fa fa-search"></i>
                                Generar
                            </button>
                        </div>
                	</form>
                </div>	
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection