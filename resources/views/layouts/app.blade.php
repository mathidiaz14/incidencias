<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>{{env("APP_EMPRESA")}}</title>

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" media="all">
    

    <link rel="icon" type="image/png" href="{{asset('images/logo.jpg')}}">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="{{url('/')}}" style="color:white;">
                            <b>{{env('APP_EMPRESA')}}</b>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                            @if(Auth::user()->tipo == "Administrador")
                                <li>
                                    <a href="{{url('reclamos')}}">
                                        <i class="fas fa-clipboard-list"></i>
                                        <span class="bot-line"></span>Reclamos
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('incidencias')}}">
                                        <i class="fas fa-file-alt"></i>
                                        <span class="bot-line"></span>Incidencias
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('categorias')}}">
                                        <i class="fas fa-boxes"></i>
                                        <span class="bot-line"></span>Categorias
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('reportes')}}">
                                        <i class="fas fa-chart-pie"></i>
                                        <span class="bot-line"></span>Reportes
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('usuarios')}}">
                                        <i class="fas fa-users"></i>
                                        <span class="bot-line"></span>Usuarios
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{url('usuario/incidencias')}}">
                                        <i class="fas fa-file-alt"></i>
                                        <span class="bot-line"></span>Incidencias
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('usuario')}}">
                                        <i class="fas fa-user"></i>
                                        <span class="bot-line"></span>Mi cuenta
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="content">
                                    <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#">{{Auth::user()->name}}</a>
                                            </h5>
                                            <span class="email">{{Auth::user()->email}}</span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__footer">    
                                        <a  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a href="{{url('/')}}" style="color:white;">
                            <b>{{env("APP_EMPRESA")}}</b>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        @if(Auth::user()->tipo == "Administrador")
                            <li>
                                <a href="{{url('usuarios')}}">
                                    <i class="fas fa-users"></i>
                                    <span class="bot-line"></span>Usuarios
                                </a>
                            </li>
                            <li>
                                <a href="{{url('recibos')}}">
                                    <i class="fas fa-file-alt"></i>
                                    <span class="bot-line"></span>Recibos de sueldo
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{url('recibos')}}">
                                    <i class="fas fa-file-alt"></i>
                                    <span class="bot-line"></span>Recibos de sueldo
                                </a>
                            </li>
                            <li>
                                <a href="{{url('usuario')}}">
                                    <i class="fas fa-user"></i>
                                    <span class="bot-line"></span>Mi cuenta
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </header>

        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                       <div class="content">
                            <a class="js-acc-btn" href="#">{{Auth::user()->nombre}}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#">{{Auth::user()->nombre}}</a>
                                    </h5>
                                    <span class="email">{{Auth::user()->email}}</span>
                                </div>
                            </div>
                            <div class="account-dropdown__footer">    
                                <a  href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="zmdi zmdi-power"></i>Cerrar Sesión</a>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <br>
            @yield('content')

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Desarrollado por <a href="http://mathiasdiaz.uy" target="_blank"> Mathias Díaz.uy </a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS -->
    <script src="{{asset('vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset('js/main.js')}}"></script>

    @yield('js')

</body>

</html>
<!-- end document-->
