@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left"></div>
                        <div class="table-data__tool-right">
                            <form action="{{url('buscar')}}" method="post" class="form-inline">
                                @csrf
                                <div class="form-group mx-sm-3 mb-2">
                                    <input type="text" class="form-control" name="incidencia" placeholder="Buscar incidencia...">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">
                                    <i class="fa fa-search"></i>
                                    Buscar
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Incidencias</h3>
                    	</div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Titulo</th>
                                    <th>Comentarios</th>
                                    <th>Edificio</th>
                                    <th>Categoria</th>
                                    <th>Asignado a</th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(Auth::user()->incidencias as $incidencia)
                                    <tr class="tr-shadow">
                                        <td>{{$incidencia->id}}</td>
                                        <td>{{$incidencia->titulo}}</td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->comentarios->count()}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->edificio_id != null ? $incidencia->edificio->nombre : "--"}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->categoria_id != null ? $incidencia->categoria->nombre : "--"}}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                @if($incidencia->user_id != null)
                                                    {{$incidencia->usuario->name}}
                                                @else
                                                    {{$incidencia->usuario_asignado}}
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <span class="block-email">
                                                {{$incidencia->created_at->format('d/m/Y H:i')}}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="{{url('incidencias', $incidencia->id)}}" class="btn btn-success">
                                                <i class="fa fa-eye"></i>
                                                Ver
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection