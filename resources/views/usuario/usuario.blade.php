@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-xs-12">
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <h3 class="title-5 m-b-35">Mis datos</h3>
                        </div>
                    </div>
                    @include('ayuda.alerta')
                    <form action="{{url('usuario')}}" method="POST" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                            <label for="nombre" class=" form-control-label">Nombre</label>
                            <input required="" type="text" value="{{Auth::user()->name}}" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email" class=" form-control-label">Email</label>
                            <input required="" type="email" id="email" value="{{Auth::user()->email}}" class="form-control" disabled="">
                        </div>
                        <hr>
                        <small>Si no completa los campos no se modificara la contraseña</small>
                        <div class="form-group">
                            <label for="contraseña" class=" form-control-label">Contraseña actual</label>
                            <input type="password" id="contrasena" placeholder="Ingrese aqui la contraseña actual" name="contraseña_actual" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="contraseña" class=" form-control-label">Nueva contraseña</label>
                            <input type="password" id="contrasena" placeholder="Ingrese aqui la nueva contraseña" name="contraseña_nueva" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="contraseña" class=" form-control-label">Repetir contraseña</label>
                            <input type="password" id="contrasena" placeholder="Ingrese aqui la nueva contraseña" name="contraseña_repetida" class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-info">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection