@extends('layouts.auth')

@section('content')
<div class="login-wrap" style="max-width: 800px!important;">
    <div class="login-content">
        <div class="login-logo">
            <h2>Ingresar reclamo</h2>
        </div>
        @include('ayuda.alerta')
        <div class="login-form">
            <form method="POST" action="{{ url('reclamos') }}">
                @csrf
                <div class="form-group">
                    <label>Nombre</label>
                    <input class="au-input au-input--full" type="text" name="nombre" placeholder="Nombre" required="">
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="au-input au-input--full" type="email" name="email" placeholder="Email" required="">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Telefono</label>
                            <input class="au-input au-input--full" type="text" name="telefono" placeholder="Telefono">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Edificio</label>
                            <select name="edificio" id="" class="au-input au-input--full" style="height: 45px;">
                                @foreach(App\Models\Edificio::all() as $edificio)
                                    <option value="{{$edificio->id}}">{{$edificio->nombre}}</option>
                                @endforeach
                                
                                <option value="Otro">Otro</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Unidad</label>
                            <input class="au-input au-input--full" type="text" name="unidad" placeholder="Unidad">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Reclamo</label>
                    <textarea id="" cols="30" rows="2" class="au-input au-input--full" placeholder="Reclamo" name="reclamo" required=""></textarea>
                </div>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Enviar</button>
            </form>
            <div class="row">
                <div class="col text-center">
                    <a href="{{url('login')}}" class="">Ir a la sección administrador</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
