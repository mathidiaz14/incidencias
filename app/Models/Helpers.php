<?php

function enviar_email($plantilla, $titulo, $contenido, $destinatario_nombre, $destinatario_email)
{	
	$mj = new \Mailjet\Client(
        '23a68841ab44393a370bc1fab8c34653', 
        '4793266e3e7e30f8fde8c2b8e90c1700', 
        true, 
        ['version' => 'v3.1']
    );
    
    $body = [
        'Messages' => [
            [
                'From' => [
                    'Email' => env('APP_EMAIL'),
                    'Name' => env('APP_EMPRESA'),
                ],
                'To' => [
                    [
                        'Email' => $destinatario_email,
                        'Name' => $destinatario_nombre
                    ]
                ],
                'Subject' => $titulo,
                'HTMLPart' => str_replace('{{contenido}}', $contenido, \File::get(resource_path('views/email/'.$plantilla.'.blade.php')))
            ]
        ]
    ];
    $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);
    $response->success();

    return $response->getData()['Messages'][0]['Status'];
}

?>