<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    use HasFactory;

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function incidencia()
    {
        return $this->belongsTo('App\Models\Incidencia', 'incidencia_id');
    }

    public function padre() 
    {
        return $this->belongsTo('App\Comentario', 'parent_id');
    }
}
