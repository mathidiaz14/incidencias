<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    use HasFactory;

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function creador()
    {
        return $this->belongsTo('App\Models\User', 'creador_id');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Models\Comentario', 'incidencia_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoria');
    }

    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificio', 'edificio_id');
    }
}
