<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Edificio extends Model
{
    /**
    * The database connection used by the model.
    *
    * @var string
    */
    protected $connection = 'edificio';

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'edificios';

    use HasFactory;

    public function incidencias()
    {
        return $this->hasMany('App\Models\Incidencia');
    }
}
