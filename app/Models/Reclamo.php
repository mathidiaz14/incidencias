<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reclamo extends Model
{
    use HasFactory;

    public function edificio()
    {
        return $this->belongsTo('App\Models\Edificio', 'edificio');
    }
}
