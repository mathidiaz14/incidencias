<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Incidencia;
use App\Models\Comentario;
use App\Models\User;
use Auth;
use Mail;

class ControladorIncidencia extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidencias = Incidencia::orderByDesc('id')->paginate(50);

        return view('admin.incidencias', compact('incidencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incidencia                     = new Incidencia();
        $incidencia->titulo             = $request->titulo;
        $incidencia->cliente_nombre     = $request->cliente_nombre;
        $incidencia->cliente_email      = $request->cliente_email;
        $incidencia->cliente_telefono   = $request->cliente_telefono;
        $incidencia->contenido          = $request->contenido;
        $incidencia->user_id            = $request->usuario;
        $incidencia->usuario_asignado   = $request->usuario_asignado;
        $incidencia->categoria_id       = $request->categoria;
        $incidencia->edificio_id        = $request->edificio;
        $incidencia->save();

        $comentario                 = new Comentario();
        $comentario->user_id        = Auth::user()->id;
        $comentario->incidencia_id  = $incidencia->id;
        $comentario->contenido      = "Se creo la incidencia";
        $comentario->save();

        if($incidencia->user_id != null)
        {
            $contenido = url('incidencias', $incidencia->id);
            
            if(isset($incidencia->usuario))
            {
                enviar_email(   'asignacion_nueva_incidencia', 
                                'Se asigno nueva incidencia', 
                                $contenido, 
                                $incidencia->usuario->name, 
                                $incidencia->usuario->email
                                );   
            }
        }

        Session(['exito' => 'La incidencia se creo correctamente.']);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incidencia = Incidencia::find($id);

        return view('admin.incidencia', compact('incidencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vieja                          = Incidencia::find($id);
        
        $incidencia                     = Incidencia::find($id);
        $incidencia->titulo             = $request->titulo;
        $incidencia->cliente_nombre     = $request->cliente_nombre;
        $incidencia->cliente_email      = $request->cliente_email;
        $incidencia->cliente_telefono   = $request->cliente_telefono;
        $incidencia->contenido          = $request->contenido;
        $incidencia->user_id            = $request->usuario;
        $incidencia->usuario_asignado   = $request->usuario_asignado;
        $incidencia->categoria_id       = $request->categoria;
        $incidencia->edificio_id        = $request->edificio;
        $incidencia->save();

        $comentario                 = new Comentario();
        $comentario->user_id        = Auth::user()->id;
        $comentario->incidencia_id  = $incidencia->id;
        $comentario->contenido      = "Se actualizo la incidencia";
        $comentario->save();

        if($vieja->user_id != $incidencia->user_id)
        {
            if($incidencia->user_id != null)
            {
                $contenido = url('incidencias', $incidencia->id);
                enviar_email('asignacion_nueva_incidencia', 'Se asigno nueva incidencia', $contenido, $incidencia->usuario->name, $incidencia->usuario->email);
            }
        }

        Session(['exito' => 'La incidencia se modifico correctamente.']);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidencia = Incidencia::find($id);
        $incidencia->delete();

        Session(['exito' => "La incidencia se elimino correctamente."]);

        return back();
    }

    public function cerrar_incidencia(Request $request)
    {
        $incidencia = Incidencia::find($request->incidencia);

        if($incidencia->estado == "activo")
        {
            if((Auth::user()->tipo == "Administrador") or (Auth::user()->id == $incidencia->user_id))
            {
                $incidencia->estado = "inactivo";
                $incidencia->save();

                $comentario = new Comentario();
                $comentario->user_id = Auth::user()->id;
                $comentario->incidencia_id = $incidencia->id;
                $comentario->contenido = "Se cerro la incidencia";
                $comentario->save();

                enviar_email('incidencia_cerrada_cliente', 'Se resolvio incidencia Nº'. $incidencia->id, $incidencia->id, $incidencia->cliente_nombre, $incidencia->cliente_email);

                if($incidencia->user_id != null)
                {
                    enviar_email('incidencia_cerrada', 'Se resolvio incidencia Nº'. $incidencia->id, $incidencia->id, $incidencia->usuario->name, $incidencia->usuario->email);
                }

                if(Auth::user()->tipo != "Administrador")
                {
                    enviar_email('incidencia_cerrada', 'Se resolvio incidencia Nº'. $incidencia->id, $incidencia->id, env('APP_EMPRESA'), env('APP_EMAIL'));
                }

                Session(['exito' => "La incidencia se cerro correctamente."]);
                return back();
            }
        }
        
        Session(['error' => "No se puso cerrar la incidencia."]);
        return back();

    }

    public function abrir_incidencia($id)
    {
        $incidencia = Incidencia::find($id);

        if($incidencia->estado == "inactivo")
        {
            $incidencia->estado = "activo";
            $incidencia->save();

            $comentario = new Comentario();
            $comentario->user_id = Auth::user()->id;
            $comentario->incidencia_id = $incidencia->id;
            $comentario->contenido = "Se re-abrio la incidencia";
            $comentario->save();

            Session(['exito' => "La incidencia se abrio correctamente."]);
        }else
        {
            Session(['error' => "No se puso abrir la incidencia."]);
        }        

        return back();
    }

    public function incidencias_usuario($id)
    {
        $usuario = User::find($id);
        $incidencias = Incidencia::where('user_id', $id)->paginate(20);

        return view('admin.incidencias_usuarios', compact('incidencias', 'usuario'));

    }

    public function incidencias_inactivas()
    {
        $incidencias = Incidencia::where('estado', 'inactivo')->paginate(50);

        return view('admin.incidencias', compact('incidencias'));
    }

}
