<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comentario;
use Auth;
use Mail;

class ControladorComentario extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comentario                 = new Comentario();
        $comentario->incidencia_id  = $request->incidencia;
        $comentario->user_id        = Auth::user()->id;
        $comentario->contenido      = $request->comentario;
        $comentario->save();

        enviar_email('nuevo_comentario_cliente', 'Nuevo comentario en la incidencia #'.$comentario->incidencia->id, $request->comentario, $comentario->incidencia->cliente_nombre, $comentario->incidencia->cliente_email);

        if  ($comentario->incidencia->user_id != null)
        {
            enviar_email('nuevo_comentario', 'Nuevo comentario en la incidencia #'.$comentario->incidencia->id, $comentario->incidencia->id, $comentario->incidencia->usuario->name, $comentario->incidencia->usuario->email);            
        }

        Session(['exito' => 'Se envio el comentario']);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comentario = Comentario::find($id);
        $comentario->delete();

        Session(['exito' => "Se elimino el comentario"]);

        return back();
    }
}
