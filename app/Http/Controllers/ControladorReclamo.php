<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reclamo;
use App\Models\Incidencia;
use App\Models\Comentario;
use App\Models\Edificio;
use Auth;

class ControladorReclamo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->tipo == "Administrador")
            return view('admin.reclamos');

        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $edificio               = Edificio::find($request->edificio);

        $reclamo                = new Reclamo();
        $reclamo->nombre        = $request->nombre;
        $reclamo->email         = $request->email;
        $reclamo->telefono      = $request->telefono;
        $reclamo->edificio_id   = $request->edificio;

        if($edificio != null)
            $reclamo->edificio  = $edificio->nombre;
        else
            $reclamo->edificio  = "Otro";
        
        $reclamo->unidad        = $request->unidad;
        $reclamo->reclamo       = $request->reclamo;
        $reclamo->save();

        Session(['exito' => "El reclamo se genero correctamente, la administración se contactara con usted"]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reclamo            = Reclamo::find($id);
        $reclamo->estado    = "inactivo";
        $reclamo->save();

        $contenido =    "Nombre: ".$reclamo->nombre."\n".
                        "Email: ".$reclamo->email."\n".
                        "Teléfono: ".$reclamo->telefono."\n".
                        "Edificio: ".$reclamo->edificio."\n".
                        "Unidad: ".$reclamo->unidad."\n\n".
                        "Reclamo: ".$reclamo->reclamo."\n";

        $incidencia                     = new Incidencia();
        $incidencia->titulo             = "Reclamo de ".$reclamo->edificio;
        $incidencia->contenido          = $contenido;
        $incidencia->edificio_id        = $reclamo->edificio_id;
        $incidencia->user_id            = $request->usuario;
        $incidencia->categoria_id       = $request->categoria;
        $incidencia->cliente_nombre     = $reclamo->nombre;
        $incidencia->cliente_email      = $reclamo->email;
        $incidencia->cliente_telefono   = $reclamo->telefono;
        $incidencia->save();

        $comentario                 = new Comentario();
        $comentario->user_id        = Auth::user()->id;
        $comentario->incidencia_id  = $incidencia->id;
        $comentario->contenido      = "Se creo la incidencia";
        $comentario->save();

        enviar_email('creacion_nueva_incidencia', 'Se creo una incidencia', $incidencia->id, $reclamo->nombre, $reclamo->email);

        if($incidencia->user_id != null)
        {
            $contenido = url('incidencias', $incidencia->id);

            enviar_email('asignacion_nueva_incidencia', 'Se asigno nueva incidencia', $contenido, $incidencia->usuario->name, $incidencia->usuario->email);
        }

        Session(['exito' => "Se genero la incidencia a partir del reclamo"]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reclamo = Reclamo::find($id);
        $reclamo->delete();

        Session(['exito' => 'El reclamo se elimino correctamente']);

        return back();
    }
}
