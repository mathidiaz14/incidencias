<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Incidencia;
use Carbon\Carbon;
use Auth;
use Hash;
use PDF;

class ControladorPrincipal extends Controller
{
    public function incidencias()
    {
    	return view('usuario.incidencias');
    }

    public function ver_usuario()
    {
        return view('usuario.usuario');
    }

    public function guardar_usuario(Request $request)
    {
        $usuario = Auth::user();

        $usuario->name = $request->name;

        if($request->contraseña_actual != "")
        {
            if(Hash::check($request->contraseña_actual, $usuario->password))
            {
                if($request->contraseña_nueva === $request->contraseña_repetida)   
                {
                    $usuario->password = Hash::make($request->contraseña_nueva);
                    
                }else
                {
                    Session(["error" => "Las contraseña nueva no coincide con la confirmaión."]);
                    return back();
                }
            }else
            {
                Session(["error" => "Las contraseñas actual no coincide con nuestro registro."]);
                return back();
            }
        }

        $usuario->save();
        
        Session(["exito" => "La datos de usuario se modificaron correctamente."]);
        return back();
    }

    public function buscar(Request $request)
    {
        $incidencias = Incidencia::where(function ($query) use ($request) {
                                       $query->where('id', 'LIKE', '%'.$request->buscar.'%')
                                            ->orWhere('titulo', 'LIKE', '%'.$request->buscar.'%')
                                            ->orWhere('contenido', '=', $request->buscar)
                                            ->orWhere('cliente_email', '=', $request->buscar)
                                            ->orWhere('cliente_nombre', '=', $request->buscar);
                                   })
                                   ->paginate(50);

        return view('admin.incidencias', compact('incidencias'));
    }

    public function get_reportes()
    {
        return view('admin.reportes');
    }

    public function set_reportes(Request $request)
    {
        $inicio         = Carbon::create($request->inicio);
        $fin            = Carbon::create($request->fin);

        $incidencias    = Incidencia::whereBetween('created_at', [$inicio, $fin])->get();

        if($request->categoria != "todos")
            $incidencias = $incidencias->where('categoria_id', $request->categoria);

        if($request->usuario != "todos")
            $incidencias = $incidencias->where('user_id', $request->usuario);

        if($request->edificio != "todos")
            $incidencias = $incidencias->where('edificio_id', $request->edificio);

        if($request->estado != "todos")
            $incidencias = $incidencias->where('estado', $request->estado);            

        return view('admin.ver_reporte', compact('incidencias', 'request'));
    }

    public function pdf_reportes(Request $request)
    {
        $inicio         = Carbon::create($request->inicio);
        $fin            = Carbon::create($request->fin);

        $incidencias    = Incidencia::whereBetween('created_at', [$inicio, $fin])->get();

        if($request->categoria != "todos")
            $incidencias = $incidencias->where('categoria_id', $request->categoria);

        if($request->usuario != "todos")
            $incidencias = $incidencias->where('user_id', $request->usuario);

        if($request->edificio != "todos")
            $incidencias = $incidencias->where('edificio_id', $request->edificio);

        if($request->estado != "todos")
            $incidencias = $incidencias->where('estado', $request->estado);

        //return view('admin.pdf', compact('incidencias'));

        $pdf = PDF::loadView('admin.pdf', compact('incidencias'))->output();
        
        return response()->streamDownload(
             fn () => print($pdf),
             Carbon::now()->format('d-m-Y_H-m').".pdf"
        );
    }
}
