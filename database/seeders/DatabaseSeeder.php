<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'tipo' => 'Administrador',
        ]);

        DB::table('users')->insert([
            'name' => 'Mathias Díaz',
            'email' => 'diaz.mathias.14@gmail.com',
            'password' => Hash::make('123456'),
            'tipo' => 'Usuario',
        ]);
    }
}
